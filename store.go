package persistor

import (
	"fmt"
	"github.com/streadway/amqp"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"log"
	"time"
	"context"
)

var err error
func CreateMongoInstance() *mongo.Collection{
	// Database Config
	clientOptions := options.Client().ApplyURI("mongodb://localhost:27017")
	client, err = mongo.NewClient(clientOptions)

	if err != nil {
		log.Fatal("Error initiating mongo client")
	}

	//Set up a context required by mongo.Connect
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	err = client.Connect(ctx)

	if err != nil {
		log.Fatal("Error connecting to mongodb")
	}

	//To close the connection at the end
	defer cancel()

	err = client.Ping(context.Background(), readpref.Primary())
	if err != nil {
		log.Fatal("Couldn't connect to the database")
	} else {
		log.Fatal("Connected!")
	}
	db := client.Database("mrmonitor")
	return db.Collection("logs")
}


func CreateAMQPInstance() (amqp.Queue, *amqp.Channel){
var err error
conn, err = amqp.Dial("amqp://guest:guest@localhost:5672/")
if err != nil {
	log.Fatal("Error connecting to Rabbitmq")
}

	ch, err = conn.Channel()
	if err != nil {
		log.Fatal("Error opening amqp channel")
	}


	q, err = ch.QueueDeclare(
		"logs",
		false,
		false,
		false,
		false,
		nil,
	)

	if err != nil{
		fmt.Println("Error declaring queue")
	}

	return q,ch

}
